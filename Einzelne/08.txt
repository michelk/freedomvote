Open Education ist als bildungspolitisches Anliegen zu verstehen, das Bildung und Wissen frei verfügbar und zugänglich gestalten will. Der uneingeschränkte Zugang zu Wissen und Bildung spielt bei den Digitalisierungs- und Virtualisierungstrends eine wesentliche Rolle um Chancengleichheit für alle zu ermöglichen. Konkret sollten dazu alle Lehrmittel unter freien Lizenzen wie Creative Commons veröffentlicht sein.
________________	

Sollen alle öffentlich finanzierten Lehrmittel (z.B. SRF Schulfernsehen) digital und kostenlos unter einer freien Lizenz zugänglich sein (Open Education)?

(Gegenteil: Alle Institutionen erstellen oder kaufen ihre eigenen Lehrmittel ein.)
________________
	  	
________________
Open Education
