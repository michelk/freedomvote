Offene (Anonymisierte) Behördendaten eröffnen vielfältige Möglichkeiten: Bürgerinnen und Bürger können die Handlungen von Regierung und Verwaltung auf Basis von Daten besser verstehen und nachvollziehen. Verwaltungen können bei der Produktion und Nutzung von Daten neue Möglichkeiten der Zusammenarbeit nutzen. Wissenschaftlerinnen und Wissenschaftler und Forschende können die bereits vorhandenen Daten nutzen, um neue Forschungsergebnisse zu erzielen. Konsumentinnen und Konsumenten können datengestützt bessere Entscheidungen bei der Auswahl von Produkten und Dienstleistungen treffen. Unternehmen können neue Produkte und Dienstleistungen konzipieren und tragen damit entscheidend zur Innovationsleistung eines Landes bei.
________________

Sollen Behörden gesammelte Daten und Statistiken nur zahlenden Kunden zur Verfügung stellen?

(Gegenteil: Kostenfreie Publikation von nicht personenbezogener und nicht sicherheitsrelevanter Daten der öffentlichen Hand (Open Government Data) )
________________
https://opendata.swiss/de/organization/bundesamt-fur-meteorologie-und-klimatologie-meteoschweiz
https://opendata.swiss/de/organization/bundesamt-fur-statistik-bfs
________________
Opendata
