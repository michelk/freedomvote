Wenn die öffentliche Hand etwas einkauft oder in Auftrag gibt, spricht man von Beschaffung. Schätzungen gehen davon aus, dass der Bund jährlich rund 1,2 Milliarden CHF für IT-Lösungen sowie Services ausgibt und damit der grösste IT-Beschaffer der Schweiz ist. Rechnet man die Beschaffungen des Bundes, der Kantone und Gemeinden für Dienstleistungen, Verteidigung, Bau und Infrastruktur hinzu, werden Investitionen im Wert von bis zu 50 Milliarden CHF getätigt.

Gut die Hälfte aller Aufträge vergibt die öffentliche Hand freihändig, also nach eigenem Gutdünken ohne vorherige Ausschreibung. Dies ist der unklaren Gesetzgebung und der Möglichkeit Aufträge in so kleine Teile zu splitten, dass diese nichtmehr den strengeren Regeln entsprechen müssen geschuldet. Durch mehr Transparenz im Vergabeprozedere, die Aufnahme von Nachhaltigkeitskriterien für Zuschläge und die Einschränkung der Ausnahmebedingungen im Bundesgesetz über das öffentliche Beschaffungswesen (BöB), hätten auch kleinere KMUs und Freie Software Anbieter mehr Chancen Aufträge zu erhalten.
________________	

Sollen öffentliche Stellen die Möglichkeit haben in gewissem Umfang frei zu entscheiden ob sie Aufträge öffentlich ausschreiben oder nicht?

(Gegenteil: Soll die freihändige Vergabe in der öffentlichen Beschaffung eingeschränkt werden?)
________________	

https://transparency.ch/oeffentliches-beschaffungswesen/
________________
OpenGov
