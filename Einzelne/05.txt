Der Staat muss bei schweren Verbrechen mit einem richterlichen Beschluss bestimmte Personen überwachen dürfen. 
Zeitgemässe Überwachungsbefugnisse bestehen und zusätzliche werden mit dem Bundesgesetzes betreffend Überwachung des Post- und Fernmeldeverkehrs (BÜPF) 2018 eingeführt. 

Im Gegensatz zur gezielten Überwachung stellt es jedoch einen unverhältnismässigen Eingriff in das Menschenrecht auf Privatsphäre dar, da es erlaubt ist, sämtliche Kommunikation (inklusive von Berufsgeheimnisträgern) präventiv zu überwachen und abzuspeichern. Erschwerend wirkt, dass diese Daten auch dem Geheimdienst zur Verfügung stehen und bei Straftaten im Internet zur Identifizierung kein Richter zustimmen muss und kein einschränkender Deliktskatalog gilt.

Es gilt zu bedenken ob man den Betroffenen Institutionen bzw. deren Mitarbeitenden auch in Zukunft sicher vorbehaltlos vertrauen kann und die  Daten nicht in falsche Hände gelangen kann.

________________	

Finden Sie, dass die anlasslose, flächendeckende und verdachtsunabhängige Überwachung und Speicherung aller Kommunikationsdaten in der Schweiz auf Vorrat durch den Staat eingeschränkt werden sollte?

(Gegenteil: Behörden im in (und unter Umständen Ausland) sollen die Möglichkeit haben alle Bürger und ihre Kommunikation jederzeit abzuhören und aufzuzeichnen.
________________	

https://de.wikipedia.org/wiki/Internet%C3%BCberwachung_in_der_Schweiz
________________	
überwachung
